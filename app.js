const express = require('express');
var app = express();


app.use(express.json());


class User {
    constructor(id, name, password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }
}


var users = [
    new User(1, 'Dimon', 'Karpov'),
    new User(2, 'Dimon2', 'Karpov2')
];


function echoString(messageString) {
    let i = 0;
    let echoMessageString = '';
    while (i < messageString.length) {
        echoMessageString = echoMessageString +
            messageString.substring(0, messageString.length - i) +
            '...' + ' ';
        i++;
    }
    return echoMessageString;
}


app.get('/users', (req, res) => {
    res.send(users);
});


app.post('/user', (req, res) => {
    let user = new User(Number(users.length + 1), req.body.name, req.body.password);
    users.push(user);
    if (users.length + 1) {
        res.status(200).json(user);
    } else {
        res.status(500).json({ status: "error" });
    }
});


app.delete('/user/:id', (req, resp) => {
    let userIndex = users.findIndex(user => user.id === Number(req.params.id));
    if (userIndex >= 0) {
        let userId = users[userIndex].id;
        users.splice(userIndex, 1)
        resp.status(200).json(userId);
    } else {
        res.status(500).json({ status: "error" });
    }
});


app.put('/user/:id', (req, resp) => {
    let user = users.find(user => user.id === Number(req.params.id));
    if (user) {
        user.name = req.body.name;
        resp.status(200).json(user);
    } else {
        res.status(500).json({ status: "error" });
    }
});


app.get('/echo', function (req, resp) {
    let messageString = req.query.message;
    resp.end(JSON.stringify(echoString(messageString)));
});


app.post('/echo', function (req, resp) {
    let messageString = req.body.message;
    resp.end(JSON.stringify(echoString(messageString)));
});


app.listen(3001, () => {
    console.log('server start');
});